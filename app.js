var express = require('express');
var app = express();
var socket = require('socket.io');
var http = require('http');
var path = require("path");
var servidor = http.createServer(app);
var io = socket.listen(servidor);
var SerialPort = require("serialport");
var ReadLine = SerialPort.parsers.Readline;

var port = new SerialPort('/dev/ttyUSB0',{
  baudRate:9600
});
port.on('data',(data)=>{
  var datos = data.toString().split(" ");
  io.emit('arduino:data',{
    nivelJugador1:datos[0],
    puntosJugador1:datos[1],
    erroresJugador1:datos[2],
    nivelJugador2:datos[3],
    puntosJugador2:datos[4],
    erroresJugador2:datos[5]
    });
});

app.set("view engine","ejs");
app.use(express.static(path.join(__dirname, 'public')));
app.get("/",function(req,res){
  res.render("index");
});

servidor.listen(3002,function(){
  console.log("Servidor escuchando");
})
